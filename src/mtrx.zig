const std = @import("std");
const math = std.math;

pub const Mx4 = struct {
    const Self = @This();

    mx: [4][4]f32 = .{
        .{ 1.0, 0.0, 0.0, 0.0 },
        .{ 0.0, 1.0, 0.0, 0.0 },
        .{ 0.0, 0.0, 1.0, 0.0 },
        .{ 0.0, 0.0, 0.0, 1.0 },
    },

    pub fn from(mx: [4][4]f32) Self {
        return .{ .mx = mx };
    }

    pub fn flatten(self: Self) [16]f32 {
        return @bitCast([16]f32, self.mx);
    }

    pub fn multiply(self: *Self, mx: Mx4) void {
        const a = self.mx;
        const b = mx.mx;

        for (0..4) |i| {
            for (0..4) |j| self.mx[i][j] =
                a[i][0] * b[0][j] +
                a[i][1] * b[1][j] +
                a[i][2] * b[2][j] +
                a[i][3] * b[3][j];
        }
    }

    pub fn transpose(self: *Self) void {
        var res: [4][4]f32 = undefined;

        for (0..4) |i| {
            for (0..4) |j| res[j][i] = self.mx[i][j];
        }

        self.mx = res;
    }


    pub fn scale(self: *Self, fac: f32) void {
        const b = Self.from(.{
            .{ fac, 0.0, 0.0, 0.0 },
            .{ 0.0, fac, 0.0, 0.0 },
            .{ 0.0, 0.0, fac, 0.0 },
            .{ 0.0, 0.0, 0.0, 1.0 },
        });

        self.multiply(b);
    }

    pub fn translate(self: *Self, x: f32, y: f32, z: f32) void {
        const b = Self.from(.{
            .{ 1.0, 0.0, 0.0, x },
            .{ 0.0, 1.0, 0.0, y },
            .{ 0.0, 0.0, 1.0, z },
            .{ 0.0, 0.0, 0.0, 1.0 },
        });

        self.multiply(b);
    }

    pub fn rotateX(self: *Self, angle: f32) void {
        const sin = math.sin(angle);
        const cos = math.cos(angle);
        const nsn = -sin;

        const b = Self.from(.{
            .{ 1.0, 0.0, 0.0, 0.0 },
            .{ 0.0, cos, nsn, 0.0 },
            .{ 0.0, sin, cos, 0.0 },
            .{ 0.0, 0.0, 0.0, 1.0 },
        });

        self.multiply(b);
    }

    pub fn rotateY(self: *Self, angle: f32) void {
        const sin = math.sin(angle);
        const cos = math.cos(angle);
        const nsn = -sin;

        const b = Self.from(.{
            .{ cos, 0.0, sin, 0.0 },
            .{ 0.0, 1.0, 0.0, 0.0 },
            .{ nsn, 0.0, cos, 0.0 },
            .{ 0.0, 0.0, 0.0, 1.0 },
        });

        self.multiply(b);
    }

    pub fn rotateZ(self: *Self, angle: f32) void {
        const sin = math.sin(angle);
        const cos = math.cos(angle);
        const nsn = -sin;

        const b = Self.from(.{
            .{ cos, nsn, 0.0, 0.0 },
            .{ sin, cos, 0.0, 0.0 },
            .{ 0.0, 0.0, 1.0, 0.0 },
            .{ 0.0, 0.0, 0.0, 1.0 },
        });

        self.multiply(b);
    }
};

pub fn zeros() Mx4 {
    return Mx4.from(.{
        .{ 0.0, 0.0, 0.0, 0.0 },
        .{ 0.0, 0.0, 0.0, 0.0 },
        .{ 0.0, 0.0, 0.0, 0.0 },
        .{ 0.0, 0.0, 0.0, 0.0 },
    });
}
