const std = @import("std");
const mem = std.mem;
const gpu = @import("gpu");
const glfw = @import("glfw");
const ps = std.process;

const WindowOptions = struct {
    width: u32,
    height: u32,
    title: [:0]const u8,
};

pub const SurfaceData = struct {
    const Self = @This();

    surface: ?*gpu.Surface,
    swap_chain: ?*gpu.SwapChain,
    current_desc: ?gpu.SwapChain.Descriptor,
    target_desc: ?gpu.SwapChain.Descriptor,

    pub fn sync(self: *Self, device: *gpu.Device) !void {
        try self.checkConfigured();

        const has_resized = !std.meta.eql(self.current_desc, self.target_desc);

        if (self.swap_chain == null or has_resized) {
            self.swap_chain = device.createSwapChain(self.surface, &self.target_desc.?);
            self.current_desc = self.target_desc;
        }
    }

    pub fn getTextureView(self: *Self) !*gpu.TextureView {
        try self.checkConfigured();
        return self.swap_chain.?.getCurrentTextureView();
    }

    pub fn present(self: *Self) !void {
        try self.checkConfigured();
        self.swap_chain.?.present();
    }

    fn checkConfigured(self: Self) !void {
        if (self.current_desc == null or self.target_desc == null) {
            return error.SwapChainNotConfigured;
        }
    }
};

pub const Setup = struct {
    const Self = @This();

    ally: mem.Allocator,
    window: glfw.Window,
    instance: *gpu.Instance,
    surface: *gpu.Surface,
    data: *SurfaceData,
    adapter: *gpu.Adapter,
    device: *gpu.Device,
    queue: *gpu.Queue,

    pub fn getSurfaceData(self: *Self) ?*SurfaceData {
        return self.window.getUserPointer(SurfaceData);
    }

    pub fn syncSurfaceData(self: *Self) !void {
        const pl = self.getSurfaceData() orelse return error.NullSufaceData;
        try pl.sync(self.device);
    }

    pub fn deinit(self: *Self) void {
        self.ally.destroy(self.data);
        self.window.destroy();
        glfw.terminate();
    }
};

pub fn setup(ally: mem.Allocator, opts: WindowOptions) Setup {
    initializeGLFW() catch {
        std.log.err("failed to create GLFW window: {?s}\n", .{glfw.getErrorString()});
        ps.exit(1);
    };

    const window = createWindow(opts) catch {
        std.log.err("failed to create GLFW window: {?s}\n", .{glfw.getErrorString()});
        ps.exit(1);
    };

    window.setFramebufferSizeCallback(handleResize);

    const instance = gpu.createInstance(null) orelse {
        std.log.err("failed to create GPU instance \n", .{});
        ps.exit(1);
    };

    const surface = getX11Surface(instance, window);

    const data = createSurfaceData(ally, surface) catch |err| {
        std.log.err("failed to allocate SurfaceData: {}", .{err});
        ps.exit(1);
    };

    window.setUserPointer(data);

    const adapter = requestAdapter(instance, surface) catch {
        std.debug.print("failed to create GPU adapter\n", .{});
        std.process.exit(1);
    };

    const device = adapter.createDevice(null) orelse {
        std.log.err("failed to create GPU device\n", .{});
        std.process.exit(1);
    };

    device.setUncapturedErrorCallback({}, printUnhandledErrorCallback);

    const queue = device.getQueue();

    logBackendInfo(adapter);

    return .{
        .ally = ally,
        .window = window,
        .instance = instance,
        .surface = surface,
        .data = data,
        .adapter = adapter,
        .device = device,
        .queue = queue,
    };
}

pub fn configureSwapChain(sp: *Setup, swap_chain_desc: gpu.SwapChain.Descriptor) void {
    var data = sp.getSurfaceData().?;
    data.current_desc = swap_chain_desc;
    data.target_desc = swap_chain_desc;
}

fn createSurfaceData(ally: mem.Allocator, surface: *gpu.Surface) !*SurfaceData {
    const data = try ally.create(SurfaceData);

    data.* = .{
        .surface = surface,
        .swap_chain = null,
        .current_desc = null,
        .target_desc = null,
    };

    return data;
}

fn handleResize(win: glfw.Window, width: u32, height: u32) void {
    const pl = win.getUserPointer(SurfaceData);
    pl.?.target_desc.?.width = width;
    pl.?.target_desc.?.height = height;
}

fn initializeGLFW() !void {
    glfw.setErrorCallback(errorCallback);
    const is_init = glfw.init(.{});
    if (!is_init) return error.AbortSetup;
}

fn createWindow(opts: WindowOptions) !glfw.Window {
    return glfw.Window.create(
        opts.width,
        opts.height,
        opts.title,
        null,
        null,
        .{},
    ) orelse error.AbortSetup;
}

fn getX11Surface(instance: *gpu.Instance, window: glfw.Window) *gpu.Surface {
    const glfw_native = glfw.Native(.{
        .x11 = true,
        .wayland = true,
    });

    // TODO: get wayland to work
    const surface_desc: gpu.Surface.Descriptor = .{
        .next_in_chain = .{
            .from_xlib_window = &.{
                .display = glfw_native.getX11Display(),
                .window = glfw_native.getX11Window(window),
            },
        },
    };

    return instance.createSurface(&surface_desc);
}

const RequestAdapterResponse = struct {
    status: gpu.RequestAdapterStatus,
    adapter: *gpu.Adapter,
    message: ?[*:0]const u8,
};

fn requestAdapter(instance: *gpu.Instance, surface: *gpu.Surface) !*gpu.Adapter {
    const adapter_opts: gpu.RequestAdapterOptions = .{
        .compatible_surface = surface,
        .power_preference = .undefined,
        .force_fallback_adapter = false,
    };

    var response: ?RequestAdapterResponse = null;
    instance.requestAdapter(&adapter_opts, &response, requestAdapterCallback);

    if (response.?.status != .success) {
        std.log.err("requestAdapter: {s}\n", .{response.?.message.?});
        return error.AbortSetup;
    }

    return response.?.adapter;
}

fn logBackendInfo(adapter: *gpu.Adapter) void {
    var props = std.mem.zeroes(gpu.Adapter.Properties);
    adapter.getProperties(&props);

    std.log.info("found {s} backend on {s}\n\tadapter = {s}, {s}\n", .{
        props.backend_type.name(),
        props.adapter_type.name(),
        props.name,
        props.driver_description,
    });
}

// Callbacks

fn errorCallback(code: glfw.ErrorCode, desc: [:0]const u8) void {
    std.log.err("glfw -> {}: {s}\n", .{ code, desc });
}

inline fn requestAdapterCallback(
    context: *?RequestAdapterResponse,
    status: gpu.RequestAdapterStatus,
    adapter: *gpu.Adapter,
    message: ?[*:0]const u8,
) void {
    context.* = RequestAdapterResponse{
        .status = status,
        .adapter = adapter,
        .message = message,
    };
}

inline fn printUnhandledErrorCallback(
    _: void,
    typ: gpu.ErrorType,
    message: [*:0]const u8,
) void {
    switch (typ) {
        .validation => std.log.err("gpu: validation error: {s}\n", .{message}),
        .out_of_memory => std.log.err("gpu: out of memory: {s}\n", .{message}),
        .device_lost => std.log.err("gpu: device lost: {s}\n", .{message}),
        .unknown => std.log.err("gpu: unknown error: {s}\n", .{message}),
        else => unreachable,
    }
    std.process.exit(1);
}
