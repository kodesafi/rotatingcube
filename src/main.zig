const std = @import("std");
const mem = std.mem;
const gpu = @import("gpu");
const stp = @import("setup.zig");
const glfw = @import("glfw");
const math = std.math;
const ps = std.process;
const mx = @import("mtrx.zig");

const WIN_TITLE = "cubez";
const WIDTH = 600;
const HEIGHT = 600;

const Vertex = struct {
    position: [3]f32,
    color: [3]f32,
};

const VERTICES = [_]Vertex{
    .{ .position = .{ -1, -1, -1 }, .color = .{ 0.0, 1.0, 0.0 } }, // 0
    .{ .position = .{ -1, -1, 1 }, .color = .{ 0.0, 0.0, 1.0 } }, // 1
    .{ .position = .{ -1, 1, 1 }, .color = .{ 0.0, 1.0, 1.0 } }, // 2
    .{ .position = .{ 1, 1, -1 }, .color = .{ 0.0, 0.5, 1.0 } }, // 3
    .{ .position = .{ -1, 1, -1 }, .color = .{ 1.0, 0.0, 1.0 } }, // 4
    .{ .position = .{ 1, -1, 1 }, .color = .{ 1.0, 1.0, 0.0 } }, // 5
    .{ .position = .{ 1, -1, -1 }, .color = .{ 1.0, 0.0, 0.0 } }, // 6
    .{ .position = .{ 1, 1, 1 }, .color = .{ 1.0, 1.0, 1.0 } }, // 7
};

const INDICES = [_]u16{
    0, 1, 2,
    3, 0, 4,
    5, 0, 6,
    3, 6, 0,
    0, 2, 4,
    5, 1, 0,
    2, 1, 5,
    7, 6, 3,
    6, 7, 5,
    7, 3, 4,
    7, 4, 2,
    7, 2, 5,
};

// NOTE: required
pub const GPUInterface = gpu.dawn.Interface;

/// GOAL: Build a rotating rainbow cube using wgpu
/// TODO: Add depth buffer.
pub fn main() !void {

    // NOTE: required
    gpu.Impl.init();

    // Setup allocator
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var ally = gpa.allocator();

    var setup = stp.setup(ally, .{
        .title = WIN_TITLE,
        .width = WIDTH,
        .height = HEIGHT,
    });

    defer setup.deinit();

    // Configure SwapChain

    const fb_size = setup.window.getFramebufferSize();

    stp.configureSwapChain(&setup, .{
        .label = "basic swap chain",
        .usage = .{ .render_attachment = true },
        .format = .bgra8_unorm,
        .width = fb_size.width,
        .height = fb_size.height,
        .present_mode = .fifo,
    });

    // Configure Shaders
    const shader = @embedFile("shader.wgsl");
    const shader_module = setup.device.createShaderModuleWGSL("my shader", shader);

    const fragment = gpu.FragmentState.init(.{
        .module = shader_module,
        .entry_point = "fs_main",
        .targets = &.{.{
            .format = .bgra8_unorm,
            .blend = &gpu.BlendState{
                .color = .{ .dst_factor = .one },
                .alpha = .{ .dst_factor = .one },
            },
            .write_mask = gpu.ColorWriteMaskFlags.all,
        }},
    });

    const vb_layout = gpu.VertexBufferLayout.init(.{
        .array_stride = @sizeOf(Vertex),
        .attributes = &[_]gpu.VertexAttribute{
            .{
                .format = .float32x3,
                .offset = 0,
                .shader_location = 0,
            },
            .{
                .format = .float32x3,
                .offset = @sizeOf([3]f32),
                .shader_location = 1,
            },
        },
    });

    const vertex = gpu.VertexState.init(.{
        .module = shader_module,
        .entry_point = "vs_main",
        .buffers = &[_]gpu.VertexBufferLayout{vb_layout},
    });

    // Create Buffers.

    const vertex_buffer = setup.device.createBuffer(&.{
        .label = "Vertex Buffer",
        .usage = .{
            .vertex = true,
            .copy_dst = true,
        },
        .size = @sizeOf(Vertex) * VERTICES.len,
    });

    setup.queue.writeBuffer(vertex_buffer, 0, &VERTICES);

    const index_buffer = setup.device.createBuffer(&.{
        .label = "Index Buffer",
        .usage = .{
            .index = true,
            .copy_dst = true,
        },
        .size = @sizeOf(u16) * INDICES.len,
    });

    setup.queue.writeBuffer(index_buffer, 0, &INDICES);

    const camera_buffer = setup.device.createBuffer(&.{
        .label = "Camera Buffer",
        .usage = .{
            .uniform = true,
            .copy_dst = true,
        },
        .size = @sizeOf([4][4]f32),
    });

    var iden: mx.Mx4 = .{};
    setup.queue.writeBuffer(camera_buffer, 0, &iden.flatten());

    // Set up Bind groups

    const camera_layout_desc = gpu.BindGroupLayout.Descriptor.init(.{
        .label = "Camera bind group layout",
        .entries = &[_]gpu.BindGroupLayout.Entry{.{
            .binding = 0,
            .visibility = .{
                .vertex = true,
            },
            .buffer = .{
                .type = .uniform,
                .has_dynamic_offset = false,
                .min_binding_size = 0,
            },
        }},
    });

    const camera_bind_layout = setup.device.createBindGroupLayout(&camera_layout_desc);

    const camera_bind_desc = gpu.BindGroup.Descriptor.init(.{
        .label = "Camera bind group",
        .layout = camera_bind_layout,
        .entries = &[_]gpu.BindGroup.Entry{.{
            .binding = 0,
            .buffer = camera_buffer,
            .size = @sizeOf([4][4]f32),
        }},
    });

    const camera_bind_group = setup.device.createBindGroup(&camera_bind_desc);

    // Setup depth buffer.
    // Configure RenderPipeline

    const layout_desc = gpu.PipelineLayout.Descriptor.init(.{
        .label = "Render Pipeline Layout",
        .bind_group_layouts = &[_]*gpu.BindGroupLayout{camera_bind_layout},
    });

    const layout = setup.device.createPipelineLayout(&layout_desc);

    const pipeline = setup.device.createRenderPipeline(&.{
        .label = "render pipeline",
        .layout = layout,
        .vertex = vertex,
        .fragment = &fragment,
        .depth_stencil = null,
        .multisample = .{},
        .primitive = .{ .cull_mode = .back },
    });

    shader_module.release();

    setup.window.setKeyCallback(handleKeyPress);

    var i: u16 = 0;
    // Event Loop
    while (!setup.window.shouldClose()) {
        const angle = (math.pi / 180.0) * @intToFloat(f32, i);
        try renderFrame(&setup, .{
            .pipeline = pipeline,
            .vertices = vertex_buffer,
            .indices = index_buffer,
            .camera = camera_buffer,
            .camera_bind = camera_bind_group,
            .angle = angle,
        });

        std.time.sleep(16 * std.time.ns_per_ms);
        i +%= 1;
    }
}

const Frame = struct {
    pipeline: *gpu.RenderPipeline,
    vertices: *gpu.Buffer,
    indices: *gpu.Buffer,
    camera: *gpu.Buffer,
    camera_bind: *gpu.BindGroup,
    angle: f32,
};

fn renderFrame(setup: *stp.Setup, frame: Frame) !void {
    glfw.pollEvents();

    // Transformed = Translate * Rotate * Scale * Original Vector

    var model: mx.Mx4 = .{};
    model.translate(0.0, 0.0, 3.0);
    model.rotateY(frame.angle);
    setup.queue.writeBuffer(frame.camera, 0, &model.flatten());

    // Render pass
    try setup.syncSurfaceData();
    const data = setup.getSurfaceData().?;

    const buffer_view = try data.getTextureView();
    defer buffer_view.release();

    const encoder = setup.device.createCommandEncoder(null);
    defer encoder.release();

    const render_pass_info = gpu.RenderPassDescriptor.init(.{
        .label = "Render pass",
        .color_attachments = &.{.{
            .view = buffer_view,
            .clear_value = gpu.Color{
                .r = 0.07,
                .g = 0.07,
                .b = 0.07,
                .a = 1.0,
            },
            .load_op = .clear,
            .store_op = .store,
        }},
    });

    const pass = encoder.beginRenderPass(&render_pass_info);
    defer pass.release();

    pass.setPipeline(frame.pipeline);
    pass.setBindGroup(0, frame.camera_bind, null);
    pass.setVertexBuffer(0, frame.vertices, 0, gpu.whole_size);
    pass.setIndexBuffer(frame.indices, .uint16, 0, gpu.whole_size);
    pass.drawIndexed(INDICES.len, 1, 0, 0, 0);
    pass.end();

    var command = encoder.finish(null);
    setup.queue.submit(&[_]*gpu.CommandBuffer{command});
    try data.present();
}

fn handleKeyPress(
    win: glfw.Window,
    key: glfw.Key,
    scancode: i32,
    action: glfw.Action,
    mods: glfw.Mods,
) void {
    if (key == .q) win.setShouldClose(true);
    _ = mods;
    _ = action;
    _ = scancode;
}
