struct VertexInput {
    @location(0) position: vec3<f32>,
    @location(1) color: vec3<f32>,
}

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) color: vec3<f32>,
};

struct CameraUniform {
    proj: mat4x4<f32>,
}

@group(0) @binding(0) var<uniform> camera: CameraUniform;

@vertex 
fn vs_main( model: VertexInput ) -> VertexOutput {
    var ans = vec4<f32>(model.position, 1.0) * camera.proj;
    ans.x = (2 * ans.x) / (ans.z + 2);
    ans.y = (2 * ans.y) / (ans.z + 2);
    ans.z = 0.0;

    var out: VertexOutput;
    out.color = model.color;
    out.clip_position = ans;
    return out;
}

@fragment 
fn fs_main( in: VertexOutput ) -> @location(0) vec4<f32> {
    return vec4<f32>(in.color, 1.0);
}
